# Setting Up Environtment Windows
A. Setting Host
1. Setting host buka notepad dengan run as administrator 
2. open file di (C:windows/system32/drivers/etc/hosts) All extensions Files(*.*);
3. buat 	127.0.0.1	pemilu.local (example) save

B. Setting Virtual
1. buka file C:\xampp\apache\conf\extra\httpd-vhosts.conf
2. paste

<VirtualHost *:80>
    ServerName pemilu.local
    ServerAlias pemilu.local

    DocumentRoot "C:\xampp\htdocs\pemilu-web"
    <Directory "C:\xampp\htdocs\pemilu-web">
       Options Indexes FollowSymLinks MultiViews

       AllowOverride All

      Order allow,deny

      allow from all

      Require all granted
    </Directory>
</VirtualHost>

3. restart apache
4. create databse di phpmyadmin dengan nama cms
5. Import database di /database cms.local
