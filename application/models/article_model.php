<?php


class article_model extends CI_Model
{

   

   /*
    * Get Menu Items
    */
    public function get_menu_items()
    {
        $this->db->where('in_menu', 1);
        $this->db->order_by('position');
        $query = $this->db->get('articles');
        return $query->result();
    }


    /*
     * Get Categories
     *
     * @param - (string) $order_by
     * @param - (string) $sort
     * @param - (int) $limit
     * @param - (int) $offset
     *
     */
    public function get_categories($order_by = null, $sort = 'DESC', $limit = null, $offset = 0)
    {
        $this->db->select('*');
        $this->db->from('categories');

        if($limit != null)
        {
            $this->db->limit($limit, $offset);
        }
        if($order_by != null)
        {
            $this->db->order_by($order_by, $sort);
        }
        $query = $this->db->get();
        return $query->result();
    }


    /*
     * Get Single Category
     */
    public function get_category($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get('categories');
        return $query->row();
    }


    /*
     * Insert Article
     *
     * @param - (array) $data
     */
    public function insert($data)
    {
        $this->db->insert('articles', $data);
        return true;
    }


    /*
     * Update Article
     *
     * @param - (array) $data
     * @param - (int) $id
     */
    public function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('articles', $data);
        return true;
    }


   /*
    * Publish Article
    *
    * @param - (int) $id
    */
    public function publish($id)
    {
        $data = array(
            'is_published' => 1
        );

        $this->db->where('id', $id);
        $this->db->update('articles', $data);
    }


   /*
    * Unpublish Article
    *
    * @param - (int) $id
    */
    public function unpublish($id)
    {
        $data = array(
            'is_published' => 0
        );

        $this->db->where('id', $id);
        $this->db->update('articles', $data);
    }


   /*
    * Delete Article
    *
    * @param - (int) $id
    */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('articles');
        return true;
    }


    /*
     * Insert Category
     *
     * @param - (array) $data
     */
    public function insert_category($data)
    {
        $this->db->insert('categories', $data);
        return true;
    }


    /*
     * Update Category
     *
     * @param - (array) $data
     * @param - (int) $id
     */
    public function update_category($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
        return true;
    }


   /*
    * Delete Category
    *
    * @param - (int) $id
    */
    public function delete_category($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('categories');
        return true;
    }


}